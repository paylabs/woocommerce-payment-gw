# PayLabs para  WooCommerce

Este Plugin  permiete la recepcion de pagos mediante tarjetas de crédito/debito de una manera facil y rapida. Para poder iniciar, es necesario contar con un registro en la la plataforma de https//pay-labs.com.

# Instalacion

En primer lugar tenemos que cumplir unos requisitos previos para poder realizar la instalación de PayLabs para WooCommerce. Necesitamos tener WooCommerce instalado. Además es recomendable tener WordPress actualizado a la última versión disponible. Después hay que seguir los siguientes pasos para realizar la instalación del plugin PayLabs para WooCommerce.

- Paso 1 – Descargar el plugin a la carpeta de Wordpress /wp-content/plugins
Mantener el nombre de 'paylabs-for-woocommerce' de la carpeta 

- Paso 2 – Activar Plugin
Cuando el modulo se haya activado ir a la página que contiene todos los plugins de WooCommerce instalados. Veremos en la lista el plugin PayLabs para WooCommerce con un botón morado y el texto “Activar”. Tenemos que hacer clic en este botón para que WooCommerce complete la instalación de las páginas necesarias para la operativa del nuevo plugin.


Paso 6 – Verificar la instalación de PayLabs para WooCommerce
Una vez que hemos realizados todos los pasos de este tutorial podemos verificar que la instalación de PayLabs para WooCommerce se ha completado con éxito, si nos dirigimos al apartado Settings de WooCommerce. Aquí veremos en el apartado de Payments la lista de formas de pagos y aparecera PayLabs para WooCommerce.

# Configuración

- Modo de Pruebas
    - Al estar activado, todas las transacciones seran dirigidas al sandbox de PayLabs y no tendran un cobro real. Aqui se pueden utilizar tarjetas de prueba ej. 41111111111111

- País de pagos
    - Todas las transacciones deben estar dirigidas a un país en especifico, y segun la tienda en linea se debe dejar uno predefinido.

- Tasa de cambio
    -  Todas las transacciones son enviadas en dolares americanos USD, por lo que si se utiliza una moneda loca, aqui se define la tasa de cambio.

- Access Token, API Key, Company Code
    - Valores obtenidos por medio de https://pay-labs.com technicalsupport@pay-labs.com