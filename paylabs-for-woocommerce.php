<?php
/*
Plugin Name: Paylabs for Woocommerce
Plugin URI: https://pay-labs.com
Description: Realiza pagos de tus productos en toda latinoamerica.
Version: 1.1
Author: Pay-Labs Team
Author URI: https://pay-labs.com/
*/
  // Include our Gateway Class and Register Payment Gateway with WooCommerce
  add_action( 'plugins_loaded', 'pl_payment_init', 0 );
  
  function pl_payment_init() {
    // If the parent WC_Payment_Gateway class doesn't exist
    // it means WooCommerce is not installed on the site
    // so do nothing
    if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
    
    // If we made it this far, then include our Gateway Class
    require_once plugin_dir_path( __FILE__ ) . 'includes/wc-pl-payment.php';
     
    // Now that we have successfully included our class,
    // Lets add it too WooCommerce
    add_filter( 'woocommerce_payment_gateways', 'add_pl_payment_gateway' );
    function add_pl_payment_gateway( $methods ) {
      $methods[] = 'PayLabs_Payment_Gateway';
      return $methods;
    }
  }
  // Add custom action links
  add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'pl_payment_action_links' );
  function pl_payment_action_links( $links ) {
    $plugin_links = array(
      '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Settings', 'pl-payment' ) . '</a>',
    );
    // Merge our new link with the default ones
    return array_merge( $plugin_links, $links );
  }