<?php

/* Authorize.net AIM Payment Gateway Class */
class PayLabs_Payment_Gateway extends WC_Payment_Gateway {

  // Setup our Gateway's id, description and other values
  function __construct() {
    // The global ID for this Payment method
    $this->id = "pl_payment";
    // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
    $this->method_title = __( "PayLabs Payment Gateway", 'pl-payment' );
    // The description for this Payment Gateway, shown on the actual Payment options page on the backend
    $this->method_description = __( "PayLabs Payment Gateway Plug-in for WooCommerce", 'pl-payment' );
    // The title to be used for the vertical tabs that can be ordered top to bottom
    $this->title = __( "PayLabs Payment Gateway", 'pl-payment' );
    // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
    $this->icon = null;
    // Bool. Can be set to true if you want payment fields to show on the checkout 
    // if doing a direct integration, which we are doing in this case
    $this->has_fields = true;    
    // This basically defines your settings which are then loaded with init_settings()
    $this->init_form_fields();
    // After init_settings() is called, you can get the settings and load them into variables, e.g:
    // $this->title = $this->get_option( 'title' );
    $this->init_settings();
    $this->sandbox = $this->get_option('sandbox');
    
    //DeviceFingerprint
    wp_enqueue_script('device_finger_print', plugin_dir_url(__FILE__) . 'cybs_devicefingerprint.js', [], '1.0', TRUE);
    
    wp_localize_script( "device_finger_print", "sandbox", $this->sandbox );
    //die();
    // Turn these settings into variables we can use
    foreach ( $this->settings as $setting_key => $value ) {
      $this->$setting_key = $value;
    }
    
    // Lets check for SSL
    add_action( 'admin_notices', array( $this,  'do_ssl_check' ) );
    
    // Save settings
    if ( is_admin() ) {
      // Versions over 2.0
      // Save our administration options. Since we are not going to be doing anything special
      // we have not defined 'process_admin_options' in this class so the method in the parent
      // class will be used instead
      add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
      // Checks if client_id is not empty.
        if ( empty( $this->exchange_rate ) || empty( $this->access_token ) || empty( $this->api_key ) || empty( $this->company_code ) ) {
            add_action( 'admin_notices', array( $this, 'client_id_missing_message' ) );
        }
    }   
  } // End __construct()
  // Build the administration fields for this specific Gateway
  public function init_form_fields() {
    $this->form_fields = array(
      'enabled' => array(
        'title'   => __( 'Activar / Desactivar', 'pl-payment' ),
        'label'   => __( 'Activar este metodo de pago', 'pl-payment' ),
        'type'    => 'checkbox',
        'default' => 'no',
      ),
      'sandbox' => array(
        'title'   => __( 'Modo de pruebas', 'pl-payment' ),
        'label'   => __( 'Activar modo de pruebas (sandbox)', 'pl-payment' ),
        'type'    => 'checkbox',
        'default' => 'yes',
      ),
      'country_option' => array(
          'title'       => __( 'Pais de pagos', 'pl-payment' ),
          'type'        => 'select',
          'description' => __( 'Pais por defecto para pagos.', 'woocommerce' ),
          'default'     => 'html',
          'class'       => 'country_option wc-enhanced-select',
          'options'     => $this->get_country_options(),
          'desc_tip'    => true,
      ),
      'title' => array(
        'title'   => __( 'Título a mostrar', 'pl-payment' ),
        'type'    => 'text',
        'desc_tip'  => __( 'Titulo que el cliente verá durante el proceso de pago.', 'pl-payment' ),
        'default' => __( 'Tarjeta de crédito', 'pl-payment' ),
      ),
      'exchange_rate' => array(
        'title'   => __( 'Tasa de cambio', 'pl-payment' ),
        'type'    => 'text',
        'desc_tip'  => __( 'Tipo de cambio de moneda local a USD$', 'pl-payment' ),
        'default' => __( '1', 'pl-payment' ),
      ),
      'description' => array(
        'title'   => __( 'Descripción', 'pl-payment' ),
        'type'    => 'textarea',
        'desc_tip'  => __( 'Descripción que el cliente verá durante el proceso de pago.', 'pl-payment' ),
        'default' => __( 'Ingresa tus datos de tarjeta, luego de pagar nos comunicaremos contigo para coordinar la entrega del producto en un transcurso de 48 horas hábiles.', 'pl-payment' ),
        'css'   => 'max-width:350px;'
      ),
      'description2' => array(
        'title'   => __( 'Descripción Line 2', 'pl-payment' ),
        'type'    => 'textarea',
        'desc_tip'  => __( 'Descripción opcional que el cliente verá durante el proceso de pago.', 'pl-payment' ),
        'default' => __( '¿Necesitas ayuda? Llama al 21212121', 'pl-payment' ),
        'css'   => 'max-width:350px;'
      ),
      'access_token' => array(
        'title'   => __( 'Access Token', 'pl-payment' ),
        'type'    => 'text',
        'desc_tip'  => __( 'Access Token de seguridad obtenida de PayLabs.', 'pl-payment' ),
        'default' => '',
      ),
      'api_key' => array(
        'title'   => __( 'Api key', 'pl-payment' ),
        'type'    => 'text',
        'desc_tip'  => __( 'Llave de api obtenida de PayLabs.', 'pl-payment' ),
        'default' => '',
      ),
      'company_code' => array(
        'title'   => __( 'Company code', 'pl-payment' ),
        'type'    => 'text',
        'desc_tip'  => __( 'Id obtenida de PayLabs.', 'pl-payment' ),
        'default' => '',
      ),
    );    
  }

public function get_country_options() {
    return array("gt" => 'Guatemala', "hn"=> 'Honduras', "sv"=> 'El Salvador', "cr"=>'Costa Rica', "pa"=>'Panama', "co"=>'Colombia');
}

public function payment_fields() {
    
    $currentMonth = date('m');
    ?>
		<style>
        	@import url('https://dv0puso5n8a2v.cloudfront.net/paylabs/paylabs-woo.min.css');
    </style>

			<p>
        <?php echo __($this->description, 'pl-payment'); ?>				
			</p>
			<p>
				<?php echo __($this->description2, 'pl-payment'); ?>
			</p>

			<div class="formulario-tarjeta">
				<div class="area-numero">
					<div class="pay-2">
						<figure>
							<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/icon-cart.svg" alt="Icon Card">
						</figure>
					</div>
					<div class="pay-10">
						<label for="cc"><?php echo __('Número completo de la tarjeta (16 digitos)', 'pl-payment'); ?></label>
						<p id="credit_card_number_field" class="form-row validate-required">
							<input type="text" name="cc" id="cc" placeholder="0000 0000 0000 0000" maxlength="16" class="input-text numero-de-tarjeta" />
						</p>
					</div>
				</div>

				<div class="area-cal">
					<div class="pay-2">
						<figure class="card-icon col-sm-2">
							<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/icon-calendar.svg" alt="Icon Calendar">		
						</figure>
					</div>
					<div class="pay-4">
						<p>Fecha de expiración</p>
							<p id="exp_month_field" class="form-row validate-required mes-de-tarjeta">
								<label for="exp_month"><?php echo __('', 'pl-payment'); ?></label>
								<select name="exp_month" id="exp_month" class="select">									
									<?php for($i = 1; $i <= 12; $i++) { ?>
									<option <?php echo ($i == $currentMonth? " selected " : "") ?> value="<?php echo (($i < 10) ? '0' . $i : $i); ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</p>
							<p id="exp_year_field" class="form-row validate-required ano-de-tarjeta">
								<label for="exp_year"><?php echo __('', 'pl-payment'); ?></label>
								<select name="exp_year" id="exp_year" class="select custom-select">									
									<?php for($i = date('Y'); $i <= date('Y') + 5; $i++) { ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</p>
					</div>
					<div class="pay-5">
						<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/exp-instruc.jpg">
					</div>
				</div>
        <div class="area-nombre">
					<div class="pay-2">
						<figure>
							<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/icon-user.svg" alt="Icon User">
						</figure>
					</div>
					<div class="pay-10">
						<label for="card_name"><?php echo __('Nombre que aparece en la tarjeta', 'pl-payment'); ?></label>
						<p id="card_name_field" class="form-row validate-required campo-texto">
							<input type="text" name="card_name" id="card_name" placeholder="Nombre como aparece en la tarjeta" class="input-text nombre-de-tarjeta" />
						</p>
					</div>
				</div>
				<div class="ara-exp">
					<div class="pay-2">
						<figure>
							<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/icon-cvv.svg" alt="Icon CVV">
						</figure>
					</div>
					<div class="pay-4">
						<!-- <p>Código de seguridad</p> -->
						<p id="cvv_field" class="form-row validate-required codigo-de-tarjeta">
							<label for="cvv"><?php echo __('Código de seguridad', 'pl-payment'); ?></label>
							<input type="text" name="cvv" id="cvv" placeholder="000" class="input-text" minlength="3" maxlength="4" required/>
						</p>
					</div>
					<div class="pay-5">
						<img src="https://dv0puso5n8a2v.cloudfront.net/paylabs/cvv-instruc.jpg">
					</div>
				</div>
			</div>
		<?php
	} 

  // Submit payment and handle response
  public function process_payment( $order_id ) {    
    global $woocommerce;
    // Get this Order's information so that we know
    // who to charge and how much
    $customer_order = new WC_Order( $order_id );
    //PayLabs endpoint
    $environment_url = 'https://api.pay-labs.com/';
    if($this->sandbox == 'no'){
      $environment_url .= "prod";
    }else if($this->sandbox == 'yes' || !$this->sandbox){
      $environment_url .= "sandbox";
    }
    $environment_url .= "/v1/payment/checkout";
    
    $order_data = $customer_order->get_data(); // The Order data
    $access_token = $this->access_token;
    $orderid = str_replace( "#", "", $customer_order->get_order_number() );
    
    // Payload configuration
    //Arrange items
    $order_items = $customer_order->get_items();
    $itemsPayload = array();
    foreach ($order_items as $item) {
      $invProd = wc_get_product($item['product_id']);
      $itemsPayload[] = array(
            "productName" => $invProd->get_name(),
            "productCode"=> $item['product_id']? $item['product_id']: "not_defined",
            "productSKU"=> $invProd->get_sku()? $invProd->get_sku(): "not_defined",
            "unitPrice"=> round($item['total'] / $this->exchange_rate, 2),
            "quantity"=> $item['quantity'],
            "id"=>  $item['product_id']
      );      
    };

    $payload = array(
      "companyCode" => $this->company_code,
      "deviceFingerPrintSession" => $_POST['deviceFingerprintID'] ? $_POST['deviceFingerprintID'] : "",
      "access_token"  => $access_token,
      "exchangeRate"  => $this->exchange_rate,
      "customer" => array(
        "email" => $order_data['billing']['email'],
        "invoiceAddress" => array(
          "firstName" => $order_data['billing']['first_name'],
          "lastName" => $order_data['billing']['last_name'],
          "street1" => $order_data['billing']['address_1'],
          "city" => $order_data['billing']['city'],
          "country" => $this->country_option,
        )
      ),
      "creditCard" => array(
        "accountNumber" => str_replace( array(' ', '-' ), '', $_POST['cc'] ),
        "nameOnCard" => $_POST['card_name'],
        "expirationMonth" => $_POST['exp_month'] ,
        "expirationYear" => $_POST['exp_year'],
        "orderid" => $orderid,
        "cvv" => $_POST['cvv'],
        "type" => "auth",
      ),
      "order" => array(
        "amount" => round($customer_order->order_total / $this->exchange_rate, 2),
        "items" => $itemsPayload        
      )
     );
    // Send this payload to Authorize.net for processing
    $response = wp_remote_post( $environment_url, array(
      'method'    => 'POST',
      'body'      => json_encode($payload),
      'timeout'   => 60,
      'sslverify' => false,
      'data_format' => 'body',
      'headers' => array(
        'Authorization' => 'Bearer ' . $access_token,
        'Content-type' => 'application/json'
      )
    ) );
    
    if ( is_wp_error( $response ) ) {
      throw new Exception( __( 'Lo lamentamos, hubo un problema con el proceso de pago. Por favor intenta de nuevo.'.$payload, 'pl-payment' ) );
    }
    if ( empty( $response['body'] ) )
      throw new Exception( __( 'Problema interno de procesamiento de pago.', 'pl-payment' ) );
      
    // Retrieve the body's resopnse if no errors found
    $response_body = wp_remote_retrieve_body( $response );
    $resp = json_decode($response_body, true);

    if ( $resp['reasonCode'] == 100 ) {
      // Payment has been successful
      $customer_order->add_order_note( __( 'Pago con PayLabs completado.', 'pl-payment' ) );
      
      // Save transaction info
      $order_id = method_exists( $customer_order, 'get_id' ) ? $customer_order->get_id() : $customer_order->ID;
      update_post_meta($order_id , '_wc_order_pl_requestId', $resp['requestId'] );
			update_post_meta($order_id , '_wc_order_pl_amount', $resp['transactionAmount'] );
			update_post_meta($order_id , '_wc_order_pl_sandbox', $this->sandbox );
			update_post_meta($order_id , '_wc_order_pl_exchangeRate', $this->exchange_rate );
                         
      // Mark order as Paid
      $customer_order->payment_complete();
      // Empty the cart (Very important step)
      $woocommerce->cart->empty_cart();
      // Redirect to thank you page
      return array(
        'result'   => 'success',
        'redirect' => $this->get_return_url( $customer_order ),
      );
    } else {
      // Transaction was not succesful
      // Add notice to the cart
      wc_add_notice( $resp['responsetext'], 'error' );
      // Add note to the order for your reference
      $customer_order->add_order_note( 'Error: '. $resp['responsetext'] );
    }
    // Validate fields
   
  }//end process payment
  public function validate_fields() {
    return true;
  }
  
  // Check forcing SSL on checkout pages
  public function do_ssl_check() {
    if( $this->enabled == "yes" ) {
      if( get_option( 'woocommerce_force_ssl_checkout' ) == "no" ) {
        echo "<div class=\"error\"><p>". sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url( 'admin.php?page=wc-settings&tab=checkout' ) ) ."</p></div>";  
      }
    }   
  }

  protected function admin_url() {
			if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '>=' ) ) {
					return admin_url( 'admin.php?page=wc-settings&tab=checkout&section=paylabs_payment_gateway' );
			}

			return admin_url( 'admin.php?page=woocommerce_settings&tab=payment_gateways&section=PayLabs_Payment_Gateway' );
	 }

  public function client_id_missing_message() {
			echo '<div class="error"><p><strong>' . __( 'Paylabs not configured', 'pl-payment' ) . '</strong>: ' . sprintf( __( 'You must set proper configuration. %s', 'pl-payment' ), '<a href="' . $this->admin_url() . '">' . __( 'Click to configure', 'pl-payment' ) . '</a>' ) . '</p></div>';
	}
}
/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'show_pl_info', 10, 1 );

function show_pl_info( $order ){

  $message = "PayLabs Transaction: ";
  $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id;
  echo '<p><strong> '.$message.' </p>';
  if(get_post_meta( $order_id, '_wc_order_pl_sandbox', true ) == 'yes'){
    echo '<strong> [Sandbox]';
  }
  
  echo '<p><strong>'.__(' RequestID').':</strong> ' . get_post_meta( $order_id, '_wc_order_pl_requestId', true ) . '</p>';
  echo '<p><strong>'.__(' Amount ($USD)').':</strong> ' . get_post_meta( $order_id, '_wc_order_pl_amount', true ) . '</p>';
  echo '<p><strong>'.__(' Exchange Rate').':</strong> ' . get_post_meta( $order_id, '_wc_order_pl_exchangeRate', true ) . '</p>';
}
?>