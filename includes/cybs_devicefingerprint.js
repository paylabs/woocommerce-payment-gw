
var heads = document.getElementsByTagName("head");
var sessionID = new Date().getTime();
var sessionDeviceFingerPrint = sessionID.toString();
var merchantID = 'paylabs';

if (sandbox == 'no') {
    var org_id = 'k8vif92e';
} else {
    var org_id = '1snn5n9w';
}

sessionDeviceFingerPrint += merchantID;

var scriptDFP = document.createElement('script');
scriptDFP.setAttribute('src', "https://h.online-metrix.net/fp/tags.js?org_id=" + org_id + "&session_id=" + sessionDeviceFingerPrint);
scriptDFP.setAttribute('type', 'text/javascript');
//Device finger print
var head = heads[0];
head.appendChild(scriptDFP);

var checkout_form = jQuery('form.checkout');
checkout_form.append('<input type="hidden" name="deviceFingerprintID" value="' + sessionDeviceFingerPrint + '">');
